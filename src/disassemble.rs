mod instruction;

use std::{fs::File,io::Read};
fn main() {
    let path = std::env::args().nth(1).expect("Missing path to RISC-V binary");
    let mut file = File::open(path).expect("Unable to open file");
    let mut buffer = vec![];
    file.read_to_end(&mut buffer).unwrap();
    let mut bytes = &mut buffer[..];
    let mut line = 0;
    while bytes.len() > 4 {
        let debug_bytes = [bytes[0], bytes[1], bytes[2], bytes[3]];
        if let Some((ins, new_bytes)) = instruction::BaseInstruction::decode(&mut bytes[..]) {
            bytes = new_bytes;
            println!("{:X}: {:?}", line, ins);
        } else {
            panic!("Unknown instruction: {:?}", debug_bytes)
        }
        line += 4;
    }
}